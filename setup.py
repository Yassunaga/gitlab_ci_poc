# -*- encoding: utf-8 -*-
# Source: https://packaging.python.org/guides/distributing-packages-using-setuptools/

import io
import re

from setuptools import find_packages, setup

dev_requirements = [
    'pylama',
    'pytest',
]
unit_test_requirements = [
    'pytest',
]
integration_test_requirements = [
    'pytest',
]
run_requirements = [
    'uvicorn==0.16.0',
    'fastapi==0.70.0',
    'pytest==6.2.5'
]

setup(
    name="my_project",
    version="1.0.0",
    author="automatizAI LTDA.",
    author_email="contato@automatizai.com.br",
    packages=find_packages(exclude='tests'),
    include_package_data=True,
    url="https://gitlab.com/projetos-automatizai/conass/api-scrapper-covid",
    license="COPYRIGHT",
    description="Scrapper de leitos UTI COVID",
    long_description="long_description",
    zip_safe=False,
    install_requires=run_requirements,
    extras_require={
        'dev': dev_requirements,
        'unit': unit_test_requirements,
        'integration': integration_test_requirements,
    },
    python_requires='>=3.6',
    classifiers=[
        'Intended Audience :: Information Technology',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.8'
    ],
    keywords=(),
    entry_points={
        'console_scripts': [
            'my_project = my_project.__main__:start'
        ],
    },
)
