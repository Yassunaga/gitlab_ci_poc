from my_project.main import get_products


def test_get_products():
    response = get_products()
    assert response == {"products": [1, 2, 3]}

def test_get_product_with_name():
    response = get_products(name="fake")
    assert response == {"products": "fake"}

