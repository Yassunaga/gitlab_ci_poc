FROM python:3.10.1-slim-buster

COPY . my_project/

RUN pip3 install -U pip && pip3 install -e my_project

CMD ["uvicorn", "--host=0.0.0.0", "--port=8000", "my_project.main:app"]
