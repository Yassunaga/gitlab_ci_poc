from fastapi import FastAPI
import uvicorn


app = FastAPI(title="FastAPI Test")


@app.get('/products', status_code=200)
def get_products(name: str = None):
    if name:
        return {"products": name}
    return {"products": [1, 2, 3]}


if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=9000)
